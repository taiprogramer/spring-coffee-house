package xyz.taiprogramer.coffeehouse.dto;

import lombok.Data;

@Data
public class SizeDto {
        private Integer id;
        private String name;
        private int price;
}
