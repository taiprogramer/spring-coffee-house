package xyz.taiprogramer.coffeehouse.dto;

import java.util.List;

import lombok.Data;

@Data
public class ProductDto {
        private Integer id;
        private String name;
        private String description;
        private String imageURL;
        private Integer categoryID;
        private String category;
        private List<SizeDto> sizes;
}
