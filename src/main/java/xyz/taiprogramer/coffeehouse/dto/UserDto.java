package xyz.taiprogramer.coffeehouse.dto;

import lombok.Data;

@Data
public class UserDto {
        private Integer id;
        private String username;
        private String password;
        private String name;
        private Integer genderID;
        private String gender;
        private String role;
        private Integer roleID;
        private String phone;
        private String email;
}
