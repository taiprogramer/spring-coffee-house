package xyz.taiprogramer.coffeehouse.controllers;

import lombok.Data;

@Data
public class ResponseType<T> {
        private T data;
        private String error;

        public void setData(T data) {
                this.data = data;
                error = null;
        }

        public void setError(String error) {
                this.error = error;
                data = null;
        }
}
