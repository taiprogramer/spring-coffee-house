package xyz.taiprogramer.coffeehouse.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import xyz.taiprogramer.coffeehouse.dto.ProductDto;
import xyz.taiprogramer.coffeehouse.dto.SizeDto;
import xyz.taiprogramer.coffeehouse.models.Product;
import xyz.taiprogramer.coffeehouse.models.ProductCategory;
import xyz.taiprogramer.coffeehouse.models.ProductSize;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProduct;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProductKey;
import xyz.taiprogramer.coffeehouse.services.CategoryService;
import xyz.taiprogramer.coffeehouse.services.ProductService;
import xyz.taiprogramer.coffeehouse.services.ProductSizeService;
import xyz.taiprogramer.coffeehouse.services.SizeProductService;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*")
public class ProductController {

        @Autowired
        private ModelMapper modelMapper;

        @Autowired
        private ProductService productService;

        @Autowired
        private SizeProductService sizeProductService;

        @Autowired
        private CategoryService categoryService;

        @Autowired
        private ProductSizeService productSizeService;

        @GetMapping("")
        private ResponseEntity<ResponseType<List<ProductDto>>> listProducts() {
                ResponseType<List<ProductDto>> responseType = new ResponseType<>();

                List<Product> products = productService.listAll();
                List<ProductDto> listProductDto = products.stream().map(product -> convertToDto(product))
                                .collect(Collectors.toList());

                responseType.setData(listProductDto);
                return new ResponseEntity<>(responseType, HttpStatus.OK);
        }

        @PostMapping("")
        private ResponseEntity<ResponseType<ProductDto>> createProduct(@RequestBody ProductDto productDto) {
                ResponseType<ProductDto> responseType = new ResponseType<>();
                Product product = convertToEntity(productDto);
                product = productService.saveOrUpdate(product);
                // save size product
                for (SizeDto sizeDto : productDto.getSizes()) {
                        ProductSizeProduct sizeProduct = generateSizeProductFromDto(product, sizeDto);
                        sizeProductService.saveOrUpdate(sizeProduct);
                }
                productDto = convertToDto(product);
                responseType.setData(productDto);
                return new ResponseEntity<>(responseType, HttpStatus.OK);
        }

        @GetMapping("/{id}")
        private ResponseEntity<ResponseType<ProductDto>> getProduct(@PathVariable int id) {
                ResponseType<ProductDto> responseType = new ResponseType<>();
                Product product = productService.getById(id);
                responseType.setData(convertToDto(product));
                return new ResponseEntity<>(responseType, HttpStatus.OK);
        }

        @PutMapping("/{id}")
        private ResponseEntity<ResponseType<ProductDto>> updateProduct(@PathVariable int id,
                        @RequestBody ProductDto productDto) {
                ResponseType<ProductDto> responseType = new ResponseType<>();
                Product product = convertToEntity(productDto);
                product.setId(id);
                product = productService.saveOrUpdate(product);
                // update size product
                for (SizeDto sizeDto : productDto.getSizes()) {
                        ProductSizeProduct sizeProduct = generateSizeProductFromDto(product, sizeDto);
                        sizeProductService.saveOrUpdate(sizeProduct);
                }
                responseType.setData(convertToDto(product));
                return new ResponseEntity<>(responseType, HttpStatus.ACCEPTED);
        }

        @DeleteMapping("/{id}")
        private ResponseEntity<ResponseType<String>> deleteProduct(@PathVariable int id) {
                ResponseType<String> responseType = new ResponseType<String>();
                ResponseEntity<ResponseType<String>> response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                Product product = productService.getById(id);
                List<ProductSizeProduct> sizeProducts = sizeProductService.findByProduct(product);
                try {
                        for (ProductSizeProduct size : sizeProducts) {
                                sizeProductService.delete(size.getId());
                        }
                        productService.delete(id);
                } catch (Exception e) {
                        responseType.setError("can not delete product");
                        response = new ResponseEntity<>(responseType, HttpStatus.BAD_REQUEST);
                }
                return response;
        }

        private ProductDto convertToDto(Product product) {
                ProductDto productDto = modelMapper.map(product, ProductDto.class);
                List<ProductSizeProduct> sizeProducts = sizeProductService.findByProduct(product);
                List<SizeDto> sizeDtos = new ArrayList<>();
                for (ProductSizeProduct sizeProduct : sizeProducts) {
                        SizeDto sizeDto = new SizeDto();
                        sizeDto.setId(sizeProduct.getProduct().getId());
                        sizeDto.setName(sizeProduct.getSize().getName());
                        sizeDto.setPrice(sizeProduct.getPrice());
                        sizeDtos.add(sizeDto);
                }
                productDto.setSizes(sizeDtos);
                productDto.setCategory(product.getCategory().getName());
                return productDto;
        }

        private Product convertToEntity(ProductDto productDto) {
                Product product = modelMapper.map(productDto, Product.class);
                // set category based on id
                ProductCategory category = categoryService.getById(product.getCategory().getId());
                product.setCategory(category);
                return product;
        }

        private ProductSizeProduct generateSizeProductFromDto(Product product, SizeDto sizeDto) {
                ProductSizeProduct sizeProduct = new ProductSizeProduct();
                ProductSizeProductKey key = new ProductSizeProductKey();
                key.setProductID(product.getId());
                key.setSizeID(sizeDto.getId());
                ProductSize size = productSizeService.getById(sizeDto.getId());
                sizeProduct.setId(key);
                sizeProduct.setProduct(product);
                sizeProduct.setSize(size);
                sizeProduct.setPrice(sizeDto.getPrice());
                return sizeProduct;
        }
}
