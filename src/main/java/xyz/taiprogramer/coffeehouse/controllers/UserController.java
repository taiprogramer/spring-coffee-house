package xyz.taiprogramer.coffeehouse.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import xyz.taiprogramer.coffeehouse.dto.UserDto;
import xyz.taiprogramer.coffeehouse.models.Gender;
import xyz.taiprogramer.coffeehouse.models.Role;
import xyz.taiprogramer.coffeehouse.models.User;
import xyz.taiprogramer.coffeehouse.services.GenderService;
import xyz.taiprogramer.coffeehouse.services.RoleService;
import xyz.taiprogramer.coffeehouse.services.UserService;

@RestController
@RequestMapping("/user")
@CrossOrigin(origins = "*")
public class UserController {

        @Autowired
        private UserService userService;

        @Autowired
        private GenderService genderService;

        @Autowired
        private RoleService roleService;

        @Autowired
        private ModelMapper modelMapper;

        @GetMapping("")
        public List<UserDto> getUsers(@RequestParam(value = "offset", required = true) int offset,
                        @RequestParam(value = "limit", required = true) int limit) {
                List<User> users = userService.listAll();
                return users.stream().map(user -> convertToDto(user))
                                .collect(Collectors.toList());
        }

        @PostMapping("")
        private ResponseEntity<ResponseType<UserDto>> createUser(@RequestBody UserDto userDto) {
                ResponseType<UserDto> responseType = new ResponseType<UserDto>();
                ResponseEntity<ResponseType<UserDto>> response = new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
                User user = convertToEnitity(userDto);
                try {
                        userService.saveOrUpdate(user);
                        userDto = convertToDto(user);
                        responseType.setData(userDto);
                        response = new ResponseEntity<ResponseType<UserDto>>(responseType, HttpStatus.CREATED);
                } catch (Exception e) {
                        responseType.setError("can not create new user");
                        responseType.setData(null);
                        response = new ResponseEntity<ResponseType<UserDto>>(responseType, HttpStatus.BAD_REQUEST);
                }

                return response;
        }

        @GetMapping("/{id}")
        private ResponseEntity<ResponseType<UserDto>> getUser(@PathVariable int id) {
                ResponseType<UserDto> responseType = new ResponseType<UserDto>();
                ResponseEntity<ResponseType<UserDto>> response = new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
                User user = userService.getById(id);
                responseType.setData(convertToDto(user));
                responseType.setError(null);
                response = new ResponseEntity<ResponseType<UserDto>>(responseType, HttpStatus.OK);
                return response;
        }

        @PutMapping("/{id}")
        private ResponseEntity<ResponseType<UserDto>> updateUser(@PathVariable int id,
                        @RequestBody UserDto userDto) {
                ResponseType<UserDto> responseType = new ResponseType<UserDto>();
                ResponseEntity<ResponseType<UserDto>> response = new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
                User user = convertToEnitity(userDto);
                user.setId(id);
                userService.saveOrUpdate(user);
                responseType.setData(convertToDto(user));
                response = new ResponseEntity<ResponseType<UserDto>>(responseType, HttpStatus.OK);
                return response;
        }

        @DeleteMapping("/{id}")
        private ResponseEntity<ResponseType<String>> deleteUser(@PathVariable int id) {
                ResponseType<String> responseType = new ResponseType<String>();
                ResponseEntity<ResponseType<String>> response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
                try {
                        userService.delete(id);
                } catch (Exception e) {
                        responseType.setError("can not delete user");
                        response = new ResponseEntity<>(responseType, HttpStatus.BAD_REQUEST);

                }
                return response;
        }

        private UserDto convertToDto(User user) {
                user.setPassword(null);
                UserDto userDto = modelMapper.map(user, UserDto.class);
                userDto.setGenderID(user.getGender().getId());
                userDto.setRoleID(user.getRole().getId());
                userDto.setGender(user.getGender().getName());
                userDto.setRole(user.getRole().getName());
                return userDto;
        }

        private User convertToEnitity(UserDto userDto) {
                User user = modelMapper.map(userDto, User.class);
                Gender gender = genderService.getById(user.getGender().getId());
                Role role = roleService.getById(user.getRole().getId());
                user.setGender(gender);
                user.setRole(role);
                return user;
        }
}
