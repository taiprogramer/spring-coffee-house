package xyz.taiprogramer.coffeehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
