package xyz.taiprogramer.coffeehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
