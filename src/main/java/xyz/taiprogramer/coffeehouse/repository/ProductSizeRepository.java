package xyz.taiprogramer.coffeehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.ProductSize;

public interface ProductSizeRepository extends JpaRepository<ProductSize, Integer> {

}
