package xyz.taiprogramer.coffeehouse.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.Product;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProduct;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProductKey;

public interface SizeProductRepository extends JpaRepository<ProductSizeProduct, ProductSizeProductKey> {
        List<ProductSizeProduct> findByProduct(Product product);
}
