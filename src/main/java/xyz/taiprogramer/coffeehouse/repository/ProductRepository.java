package xyz.taiprogramer.coffeehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
