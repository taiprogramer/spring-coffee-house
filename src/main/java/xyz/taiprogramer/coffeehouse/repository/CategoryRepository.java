package xyz.taiprogramer.coffeehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.ProductCategory;

public interface CategoryRepository extends JpaRepository<ProductCategory, Integer> {

}
