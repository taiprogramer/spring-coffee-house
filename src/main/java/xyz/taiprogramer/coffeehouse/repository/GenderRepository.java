package xyz.taiprogramer.coffeehouse.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.taiprogramer.coffeehouse.models.Gender;

public interface GenderRepository extends JpaRepository<Gender, Integer> {

}
