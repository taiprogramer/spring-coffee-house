package xyz.taiprogramer.coffeehouse.models;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "product_size_product")
@Data
public class ProductSizeProduct {
        @EmbeddedId
        private ProductSizeProductKey id;

        @ManyToOne
        @MapsId("productID")
        @JoinColumn(name = "product_id")
        private Product product;

        @ManyToOne
        @MapsId("sizeID")
        @JoinColumn(name = "size_id")
        private ProductSize size;

        @Column(name = "price")
        private int price;
}
