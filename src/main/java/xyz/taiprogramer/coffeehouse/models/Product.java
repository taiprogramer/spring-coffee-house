package xyz.taiprogramer.coffeehouse.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "product")
@Data
public class Product {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Integer id;

        @Column(name = "name")
        private String name;

        @Column(name = "imageURL")
        private String imageURL;

        @Column(name = "description")
        private String description;

        @ManyToOne
        @JoinColumn(name = "category_id", referencedColumnName = "id")
        private ProductCategory category;
}
