package xyz.taiprogramer.coffeehouse.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "user")
@Data
public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        private Integer id;

        @Column(name = "username", unique = true)
        private String username;

        @Column(name = "name")
        private String name;

        @Column(name = "password")
        private String password;

        @Column(name = "email", unique = true)
        private String email;

        @Column(name = "phone", unique = true)
        private String phone;

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "gender_id", referencedColumnName = "id")
        private Gender gender;

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "role_id", referencedColumnName = "id")
        private Role role;
}
