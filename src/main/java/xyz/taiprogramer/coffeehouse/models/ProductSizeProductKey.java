package xyz.taiprogramer.coffeehouse.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class ProductSizeProductKey implements Serializable {
        @Column(name = "product_id")
        private Integer productID;

        @Column(name = "size_id")
        private Integer sizeID;
}
