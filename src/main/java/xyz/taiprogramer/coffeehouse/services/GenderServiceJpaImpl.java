package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.taiprogramer.coffeehouse.models.Gender;
import xyz.taiprogramer.coffeehouse.repository.GenderRepository;

@Service
public class GenderServiceJpaImpl implements GenderService {

        @Autowired
        private GenderRepository genderRepository;

        @Override
        public List<Gender> listAll() {
                return genderRepository.findAll();
        }

        @Override
        public Gender getById(Integer id) {
                return genderRepository.getById(id);
        }

        @Override
        public Gender saveOrUpdate(Gender gender) {
                return genderRepository.save(gender);
        }

        @Override
        public void delete(Integer id) {
                genderRepository.deleteById(id);
        }
}
