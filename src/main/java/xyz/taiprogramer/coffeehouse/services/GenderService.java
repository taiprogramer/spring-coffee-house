package xyz.taiprogramer.coffeehouse.services;

import xyz.taiprogramer.coffeehouse.models.Gender;

public interface GenderService extends CRUDService<Gender> {

}
