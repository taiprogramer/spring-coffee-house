package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

public interface CRUDService<T> {
        List<T> listAll();

        T getById(Integer id);

        T saveOrUpdate(T obj);

        void delete(Integer id);
}
