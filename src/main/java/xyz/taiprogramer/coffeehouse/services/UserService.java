package xyz.taiprogramer.coffeehouse.services;

import xyz.taiprogramer.coffeehouse.models.User;

public interface UserService extends CRUDService<User> {

}
