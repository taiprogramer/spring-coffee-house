package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.taiprogramer.coffeehouse.models.Product;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProduct;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProductKey;
import xyz.taiprogramer.coffeehouse.repository.SizeProductRepository;

@Service
public class SizeProductJpaImpl implements SizeProductService {

        @Autowired
        private SizeProductRepository sizeProductRepository;

        @Override
        public List<ProductSizeProduct> listAll() {
                return sizeProductRepository.findAll();
        }

        @Override
        public ProductSizeProduct getById(ProductSizeProductKey id) {
                return sizeProductRepository.getById(id);
        }

        @Override
        public ProductSizeProduct saveOrUpdate(ProductSizeProduct sizeProduct) {
                return sizeProductRepository.save(sizeProduct);
        }

        @Override
        public void delete(ProductSizeProductKey id) {
                sizeProductRepository.deleteById(id);
        }

        @Override
        public List<ProductSizeProduct> findByProduct(Product product) {
                return sizeProductRepository.findByProduct(product);
        }
}
