package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.taiprogramer.coffeehouse.models.ProductSize;
import xyz.taiprogramer.coffeehouse.repository.ProductSizeRepository;

@Service
public class ProductSizeServiceJpaImpl implements ProductSizeService {

        @Autowired
        ProductSizeRepository productSizeRepository;

        @Override
        public List<ProductSize> listAll() {
                return productSizeRepository.findAll();
        }

        @Override
        public ProductSize getById(Integer id) {
                return productSizeRepository.getById(id);
        }

        @Override
        public ProductSize saveOrUpdate(ProductSize productSize) {
                return productSizeRepository.save(productSize);
        }

        @Override
        public void delete(Integer id) {
                productSizeRepository.deleteById(id);
        }

}
