package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.taiprogramer.coffeehouse.models.ProductCategory;
import xyz.taiprogramer.coffeehouse.repository.CategoryRepository;

@Service
public class CategoryServiceJpaImpl implements CategoryService {

        @Autowired
        private CategoryRepository categoryRepository;

        @Override
        public List<ProductCategory> listAll() {
                return categoryRepository.findAll();
        }

        @Override
        public ProductCategory getById(Integer id) {
                return categoryRepository.getById(id);
        }

        @Override
        public ProductCategory saveOrUpdate(ProductCategory category) {
                return categoryRepository.save(category);
        }

        @Override
        public void delete(Integer id) {
                categoryRepository.deleteById(id);
        }
}
