package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

public interface CRUDServiceCompositeKey<T, K> {
        List<T> listAll();

        T getById(K id);

        T saveOrUpdate(T obj);

        void delete(K id);
}
