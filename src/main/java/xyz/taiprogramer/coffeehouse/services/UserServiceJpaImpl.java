package xyz.taiprogramer.coffeehouse.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.taiprogramer.coffeehouse.models.User;
import xyz.taiprogramer.coffeehouse.repository.GenderRepository;
import xyz.taiprogramer.coffeehouse.repository.RoleRepository;
import xyz.taiprogramer.coffeehouse.repository.UserRepository;

@Service
public class UserServiceJpaImpl implements UserService {
        @Autowired
        UserRepository userRepository;

        @Autowired
        RoleRepository roleRepository;

        @Autowired
        GenderRepository genderRepository;

        @Override
        public List<User> listAll() {
                return userRepository.findAll();
        }

        @Override
        public User getById(Integer id) {
                Optional<User> optionalUser = userRepository.findById(id);
                return optionalUser.isPresent() ? optionalUser.get() : null;
        }

        @Override
        public User saveOrUpdate(User user) {
                return userRepository.save(user);
        }

        @Override
        public void delete(Integer id) {
                userRepository.deleteById(id);
        }
}
