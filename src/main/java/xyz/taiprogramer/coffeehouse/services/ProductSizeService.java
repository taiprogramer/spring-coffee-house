package xyz.taiprogramer.coffeehouse.services;

import xyz.taiprogramer.coffeehouse.models.ProductSize;

public interface ProductSizeService extends CRUDService<ProductSize> {

}
