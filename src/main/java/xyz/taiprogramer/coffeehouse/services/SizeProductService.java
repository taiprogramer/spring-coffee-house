package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

import xyz.taiprogramer.coffeehouse.models.Product;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProduct;
import xyz.taiprogramer.coffeehouse.models.ProductSizeProductKey;

public interface SizeProductService extends CRUDServiceCompositeKey<ProductSizeProduct, ProductSizeProductKey> {
        List<ProductSizeProduct> findByProduct(Product product);
}
