package xyz.taiprogramer.coffeehouse.services;

import xyz.taiprogramer.coffeehouse.models.Role;

public interface RoleService extends CRUDService<Role> {

}
