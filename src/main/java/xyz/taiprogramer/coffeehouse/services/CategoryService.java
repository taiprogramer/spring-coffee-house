package xyz.taiprogramer.coffeehouse.services;

import xyz.taiprogramer.coffeehouse.models.ProductCategory;

public interface CategoryService extends CRUDService<ProductCategory> {

}
