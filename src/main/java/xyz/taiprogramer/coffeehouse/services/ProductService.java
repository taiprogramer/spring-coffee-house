package xyz.taiprogramer.coffeehouse.services;

import xyz.taiprogramer.coffeehouse.models.Product;

public interface ProductService extends CRUDService<Product> {

}
