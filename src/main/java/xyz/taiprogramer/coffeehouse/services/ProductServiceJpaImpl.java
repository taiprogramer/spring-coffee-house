package xyz.taiprogramer.coffeehouse.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.taiprogramer.coffeehouse.models.Product;
import xyz.taiprogramer.coffeehouse.repository.ProductRepository;

@Service
public class ProductServiceJpaImpl implements ProductService {

        @Autowired
        private ProductRepository productRepository;

        @Override
        public List<Product> listAll() {
                return productRepository.findAll();
        }

        @Override
        public Product getById(Integer id) {
                return productRepository.getById(id);
        }

        @Override
        public Product saveOrUpdate(Product product) {
                return productRepository.save(product);
        }

        @Override
        public void delete(Integer id) {
                productRepository.deleteById(id);
        }
}
